#ifndef ZEPPELIN_H
#define ZEPPELIN_H

#include "common.h"
#include "Object.h"

class Zeppelin : public CObject
{

public:

	Zeppelin();
	Zeppelin(std::string pathNameOfObjFile);
	void updatePosition(Math::Vector3 position);
	void rotate(float angle);
	GLvoid draw();

public:

	const float speedMove   = 0.01f;
	const float speedRotate = 0.001f;

private:

	std::vector <Math::Vector3> changesVertices;
};

#endif /* ZEPPELIN_H */