#include "Texture.h"

void LoadTextureFile(texID *texture, LPCTSTR lpFileName)
{
	HANDLE	hFile;
	BOOL	bResult;
	DWORD	dwNumberOfBytesRead = 0;
	BYTE   *data;

	hFile = CreateFile(lpFileName, GENERIC_READ, 0, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	const DWORD dwCountHeaderBytes = 18;

	BYTE	header[dwCountHeaderBytes];
	DWORD	imageSize;

	bResult = ReadFile(hFile, header, dwCountHeaderBytes, &dwNumberOfBytesRead, NULL);

	GLuint width  = header[0x0C] + header[0x0D] * 256;
	GLuint height = header[0x0E] + header[0x0F] * 256;
	BYTE   bpp	  = header[0x10];
	GLuint type;

	if      (bpp == 24) type = GL_RGB;
	else if (bpp == 32) type = GL_RGBA;
	else     exit(0);

	BYTE bytesPerPixel = bpp / 8;
	imageSize = width * height * bytesPerPixel;
	data = new BYTE[imageSize];
	
	bResult = ReadFile(hFile, data, imageSize, &dwNumberOfBytesRead, NULL);
	CloseHandle(hFile);

	// BGRA -> RGBA
	BYTE temp;
	for (UINT i = 0; i < int(imageSize); i += bytesPerPixel)
	{
		temp    = data[i];
		data[i] = data[i + 2];
		data[i + 2] = temp;
	}
	// end.

	glGenTextures(1, texture);

	glBindTexture(GL_TEXTURE_2D, *texture);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, type, width, height, 0, 
		type, GL_UNSIGNED_BYTE, &data[0]);
	
	delete[] data;
}