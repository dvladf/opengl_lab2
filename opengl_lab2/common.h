#ifndef COMMON_H
#define COMMON_H

// WinAPI header:
#include <Windows.h>
// Standart OpenGL headers:
#include <GL/gl.h>
#include <GL/glu.h>
// OpenGL Extensions headers:
#include <GL/glext.h>
#include <GL/wglext.h>
// Standart C++ Headers:
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Math.h"

#define  MAP_SIZE      256
#define  STEP_SIZE	   16
#define  CAMERA_SPEED  0.1f

#define  MYFILE_HEIGHT_MAP				"img\\terrain.raw"
#define  MYFILE_TEXTURE_SKYBOX_BACK		"img\\skybox_back.tga"
#define  MYFILE_TEXTURE_SKYBOX_DOWN		"img\\skybox_down.tga"
#define  MYFILE_TEXTURE_SKYBOX_FRONT	"img\\skybox_front.tga"
#define  MYFILE_TEXTURE_SKYBOX_LEFT		"img\\skybox_left.tga"
#define  MYFILE_TEXTURE_SKYBOX_RIGHT	"img\\skybox_right.tga"
#define  MYFILE_TEXTURE_SKYBOX_UP		"img\\skybox_up.tga"
#define  MYFILE_TEXTURE_LANDSCAPE		"img\\landscape.tga"
#define	 MYFILE_TEXTURE_GRASS			"img\\grass.tga"

typedef unsigned char BYTE;
typedef unsigned int  UINT;

#endif /* COMMON_H */