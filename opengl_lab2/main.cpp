#include "common.h"
#include "Camera.h"
#include "Landscape.h"
#include "Skybox.h"
#include "Zeppelin.h"

#define IDI_OPENGL_ICON 107

HWND  g_hWnd = NULL;
HDC   g_hDC  = NULL;
HGLRC g_hRC  = NULL;

int width, height;

struct sFog
{
	GLfloat density;
	GLfloat color[4];
} g_sFog;

Camera    g_Camera;
BYTE      g_HeightMap[MAP_SIZE * MAP_SIZE];
Zeppelin *g_Zeppelin;

float heightSurface;

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void Init();
void ShutDown();
void Render();
void CheckForMovement();

int WINAPI WinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine,
	_In_ int nCmdShow)
{
	WNDCLASSEX winClass;
	MSG	uMsg;

	memset(&uMsg, 0, sizeof(uMsg));

	winClass.lpszClassName = "MY_WINDOW_CLASS";
	winClass.cbSize = sizeof(WNDCLASSEX);
	winClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	winClass.lpfnWndProc = WindowProc;
	winClass.hInstance = hInstance;
	winClass.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_OPENGL_ICON);
	winClass.hIconSm = LoadIcon(hInstance, (LPCTSTR)IDI_OPENGL_ICON);
	winClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	winClass.lpszMenuName = NULL;
	winClass.cbClsExtra = 0;
	winClass.cbWndExtra = 0;

	if (!RegisterClassEx(&winClass))
		return E_FAIL;

	g_hWnd = CreateWindowEx(NULL, "MY_WINDOW_CLASS",
		"OpenGL - Initialization",
		WS_OVERLAPPEDWINDOW |
		WS_VISIBLE,
		0, 0, 640, 480, NULL, NULL,
		hInstance, NULL);

	if (g_hWnd == NULL)
		return E_FAIL;

	ShowWindow(g_hWnd, nCmdShow);
	UpdateWindow(g_hWnd);

	Init();

	while (uMsg.message != WM_QUIT)
	{
		if (PeekMessage(&uMsg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&uMsg);
			DispatchMessage(&uMsg);
		}
	}

	ShutDown();

	UnregisterClass("MY_WINDOW_CLASS", winClass.hInstance);
	return uMsg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg,
	WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_PAINT:
		{
			Sleep(5);
			CheckForMovement();
			Render();
		}

		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_ESCAPE:
				{
					PostQuitMessage(0);
				}
				break;

			break;
			}
		}
		break;

		case WM_SIZE:
		{
			int nWidth  = LOWORD(lParam);
			int nHeight = HIWORD(lParam);
			width  = nWidth;
			height = nHeight;
		}
		break;

		case WM_DESTROY:
		{
			KillTimer(hWnd, 1);
			PostQuitMessage(0);
		}
		break;

		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
			break;
	}
	return 0;
}

void Init()
{
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = 16;
	pfd.cDepthBits = 16;

	g_hDC = GetDC(g_hWnd);
	GLuint iPixelFormat = ChoosePixelFormat(g_hDC, &pfd);

	if (iPixelFormat != 0)
	{
		PIXELFORMATDESCRIPTOR bestMatch_pfd;
		DescribePixelFormat(g_hDC, iPixelFormat, sizeof(pfd), &bestMatch_pfd);

		if (bestMatch_pfd.cDepthBits < pfd.cDepthBits)
			return;

		if (SetPixelFormat(g_hDC, iPixelFormat, &pfd) == FALSE)
		{
			DWORD dwErrorCode = GetLastError();
			return;
		}
	}

	else
	{
		DWORD dwErrorCode = GetLastError();
		return;
	}

	g_hRC = wglCreateContext(g_hDC);
	wglMakeCurrent(g_hDC, g_hRC);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	g_Camera.PositionCamera(MAP_SIZE / 2, 2.5f, 6 + MAP_SIZE / 2,
							MAP_SIZE / 2, 2.5f,     MAP_SIZE / 2,
							0, 1, 0);

	g_sFog.color[0] = 0.9f;		g_sFog.color[1] = 0.9f;
	g_sFog.color[2] = 1;		g_sFog.color[3] = 0;
	g_sFog.density = 0.01f;

	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_ALPHA_TEST);

	glAlphaFunc(GL_GREATER, 0);
	glEnable(GL_NORMALIZE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);
	//glEnable(GL_COLOR_MATERIAL);

	//GLfloat light_ambient[] = { 1, 1, 1, 1 };
	/*glColorMaterial(GL_FRONT, GL_AMBIENT);
	glColor3f(1, 1, 1);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glColor3f(0, 0, 0);
	glColorMaterial(GL_FRONT, GL_SPECULAR);
	glColor3f(0, 0, 0);*/
	//GLfloat light_ambient[]  = { 1, 1, 1, 1 };
	//GLfloat light_diffuse[]  = { 1, 1, 1, 1 };
	//GLfloat light_specular[] = { 1, 1, 1, 1 };
	//glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	LoadRawFile(MYFILE_HEIGHT_MAP, MAP_SIZE * MAP_SIZE, g_HeightMap);
	InitSkybox();
	InitLandscape(g_HeightMap);

	//glDisable(GL_TEXTURE_2D);
	g_Zeppelin = new Zeppelin("img\\zeppelin.obj.txt");

	g_Camera.moveCamera(FindHeight(g_HeightMap,
		g_Camera.m_vPosition.x, g_Camera.m_vPosition.z), CAMERA_SPEED);
}

void ShutDown()
{
	if (g_hRC != NULL){
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(g_hRC);
		g_hRC = NULL;
	}

	if (g_hDC != NULL){
		ReleaseDC(g_hWnd, g_hDC);
		g_hDC = NULL;
	}
}

void Render()
{
	glViewport(0, 0, width, height);

	glClearColor(.9f, .9f, 1, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluPerspective(45.0, (GLdouble)width / (GLdouble)height, 4, 4000.0f);
	gluLookAt(g_Camera.m_vPosition.x, g_Camera.m_vPosition.y, g_Camera.m_vPosition.z,
			  g_Camera.m_vView.x,	  g_Camera.m_vView.y,	  g_Camera.m_vView.z,
		      g_Camera.m_vUpVector.x, g_Camera.m_vUpVector.y, g_Camera.m_vUpVector.z);

	glMatrixMode(GL_PROJECTION);

	g_Camera.moveCamera(10.f, g_Zeppelin->speedMove);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glColor3f( 1, 1, 1 );
	glEnable(GL_TEXTURE_2D);
	RenderSkybox(g_Camera.m_vPosition.x, g_Camera.m_vPosition.y - 6.0f, 
		g_Camera.m_vPosition.z, MAP_SIZE * 1.5f, 200, MAP_SIZE * 1.5f);

	RenderSurface(g_HeightMap);

	RenderGrass(g_HeightMap, g_Camera.m_vXcoord, g_Camera.m_vYcoord);
	glDisable(GL_TEXTURE_2D);

	g_Zeppelin->updatePosition(g_Camera.m_vPosition);
	g_Zeppelin->draw();

	SwapBuffers(g_hDC);
}

void CheckForMovement()
{
	if (GetKeyState(VK_UP) & 0x80)
	{
		/*g_Camera.moveCamera(FindHeight(g_HeightMap, 
			g_Camera.m_vPosition.x, g_Camera.m_vPosition.z), CAMERA_SPEED);*/
	}

	if (GetKeyState(VK_DOWN) & 0x80)
	{
		/*g_Camera.moveCamera(FindHeight(g_HeightMap,
			g_Camera.m_vPosition.x, g_Camera.m_vPosition.z), -CAMERA_SPEED);*/
	}

	if (GetKeyState(VK_LEFT) & 0x80)
	{
		g_Camera.rotateView(g_Zeppelin->speedRotate, 0, 1, 0);
		g_Zeppelin->rotate(-g_Zeppelin->speedRotate);
	}
		
	if (GetKeyState(VK_RIGHT) & 0x80)
	{
		g_Camera.rotateView(-g_Zeppelin->speedRotate, 0, 1, 0);
		g_Zeppelin->rotate(g_Zeppelin->speedRotate);
	}
}