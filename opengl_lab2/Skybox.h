#ifndef SKYBOX_H
#define SKYBOX_H

#include "common.h"
#include "Texture.h"

void   InitSkybox();
GLvoid RenderSkybox(float x, float y, float z, float width, float height, float length);

#endif /* SKYBOX_H */