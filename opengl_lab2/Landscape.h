#ifndef TERRAIN_H
#define TERRAIN_H

#include <cstdlib>		// for rand()
#include "common.h"
#include "Texture.h"

#define COUNT_GRASS		50

void	InitLandscape(BYTE *pHeightMap);
GLvoid	RenderSurface(BYTE *pHeightMap);
GLvoid  RenderGrass(BYTE *pHeightMap, Math::Vector3 vRightVector, Math::Vector3 vUpVector);
float   FindHeight(BYTE *pHeightMap, float x, float z);
void	LoadRawFile(LPCTSTR lpFileName, DWORD dwNumberOfBytesToRead, BYTE *pBuffer);

#endif /* TERRAIN_H */