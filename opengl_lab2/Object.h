#ifndef OBJECT_H
#define OBJECT_H

#include "common.h"
#include "Texture.h"

class CObject
{

public:

	CObject();
	void   loadFromObjFile(std::string pathname);
	void   setTexture();
	GLvoid render(std::vector <Math::Vector3> changesVertices);

protected:

	struct PartObject
	{
		std::string name;
		unsigned idMaterial;
		unsigned endItemFace;
		unsigned endNumVertex;	// ��������� ����� ������� ������ ����� �������
	};

	std::vector <Math::Vector3> vertices;
	std::vector <PartObject> partsObject;

private:

	void loadMtlFile(std::string filename);

private:

	struct Material
	{
		std::string name;
		Math::Vector3 mLight_color;
		Math::Vector3 mDiffuse_color;
		Math::Vector3 mReflection_color;
		float mReflection_rate;
		float transparency;
	};

	std::vector <Math::Vector2> uvs;
	std::vector <Math::Vector3> normals;

	std::vector <unsigned> vertexIndices, uvIndices, normalIndices;

	std::vector <Material> materials;

	texID texture;

};

#endif /* OBJECT_H */