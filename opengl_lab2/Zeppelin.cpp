#include "Zeppelin.h"

Zeppelin::Zeppelin()
{
}

Zeppelin::Zeppelin(std::string pathNameOfObjFile)
{
	loadFromObjFile(pathNameOfObjFile);

	Math::Vector3 axis = { 1, 0, 0 };
	CObject::vertices = Math::rotate(CObject::vertices, -90 * Math::deg2rad, axis);

	axis = { 0, 1, 0 };
	CObject::vertices = Math::rotate(CObject::vertices, -180 * Math::deg2rad, axis);

	for (int i = 0; i < vertices.size(); i++)
	{
		CObject::vertices[i].x = 5 * CObject::vertices[i].x;
		CObject::vertices[i].y = 5 * CObject::vertices[i].y - 5;
		CObject::vertices[i].z = 5 * CObject::vertices[i].z - 70;
	}
}

void Zeppelin::updatePosition(Math::Vector3 position)
{
	changesVertices.clear();
	for (int i = 0; i < vertices.size(); i++)
	{
		Math::Vector3 changeVertex;
		changeVertex = CObject::vertices[i] + position;
		changesVertices.push_back(changeVertex);
	}
}

void Zeppelin::rotate(float angle)
{
	Math::Vector3 axis = { 0, 1, 0 };
	CObject::vertices = Math::rotate(CObject::vertices, angle, axis);

	/*std::vector <Math::Vector3> item;
	for (int i = 0; i < partsObject.size(); i++)

		if (partsObject[i].name == "Circle.003")
		{
			axis = { 0, 1, 0 };
			unsigned startNumVertex;

			if (i == 0)
				startNumVertex = 0;

			else
				startNumVertex = partsObject[i - 1].endNumVertex;

			for (unsigned j = startNumVertex; j < partsObject[i].endNumVertex; j++)
				vertices[j] = Math::rotateVertex(vertices[j], 0.1 * angle, axis);

			break;
		}*/

}

GLvoid Zeppelin::draw()
{
	CObject::render(changesVertices);
}