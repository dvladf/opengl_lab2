#include "Camera.h"

Camera::Camera()
{
	Math::Vector3 vZero = { 0.0, 0.0, 0.0 };
	Math::Vector3 vView = { 0.0, 1.0, 0.5 };
	Math::Vector3 vUp   = { 0.0, 0.0, 1.0 };

	m_vPosition = vZero;
	m_vView		= vView;
	m_vUpVector = vUp;
}

Camera::~Camera()
{
}

GLvoid Camera::PositionCamera(float positionX, float positionY, float positionZ,
	float viewX, float viewY, float viewZ,
	float upVectorX, float upVectorY, float upVectorZ)
{
	Math::Vector3 vPosition = { positionX, positionY, positionZ };
	Math::Vector3 vView	   = { viewX, viewY, viewZ };
	Math::Vector3 vUpVector = { upVectorX, upVectorY, upVectorZ };

	m_vPosition = vPosition;
	m_vView		= vView;
	m_vUpVector = vUpVector;
}

void Camera::moveCamera(float height, float speed)
{
	Math::Vector3 vVector = m_vView - m_vPosition;

	m_vPosition.x += vVector.x * speed;
	m_vPosition.y  = height + 6.0f;
	m_vPosition.z += vVector.z * speed;

	m_vView.x += vVector.x * speed;
	m_vView.y  = m_vPosition.y;
	m_vView.z += vVector.z * speed;

	if (m_vPosition.x > MAP_SIZE)
	{
		m_vPosition.x -= MAP_SIZE;
		m_vView.x -= MAP_SIZE;
	}

	if (m_vPosition.x < 0)
	{
		m_vPosition.x += MAP_SIZE;
		m_vView.x += MAP_SIZE;
	}

	if (m_vPosition.z > MAP_SIZE)
	{
		m_vPosition.z -= MAP_SIZE;
		m_vView.z -= MAP_SIZE;
	}

	if (m_vPosition.z < 0)
	{
		m_vPosition.z += MAP_SIZE;
		m_vView.z += MAP_SIZE;
	}

	findCoord();
}

void Camera::rotateView(float angle, float x, float y, float z)
{
	Math::Vector3 vNewView;
	Math::Vector3 vView;

	vView = m_vView - m_vPosition;

	float cosTheta = (float)cos(angle);
	float sinTheta = (float)sin(angle);

	vNewView.x  = (cosTheta + (1 - cosTheta) * x * x)     * vView.x;
	vNewView.x += ((1 - cosTheta) * x * y + z * sinTheta) * vView.y;
	vNewView.x += ((1 - cosTheta) * x * z + y * sinTheta) * vView.z;

	vNewView.y  = ((1 - cosTheta) * x * y + z * sinTheta) * vView.x;
	vNewView.y += (cosTheta + (1 - cosTheta) * y * y)     * vView.y;
	vNewView.y += ((1 - cosTheta) * y * z - x * sinTheta) * vView.z;

	vNewView.z  = ((1 - cosTheta) * x * z - y * sinTheta) * vView.x;
	vNewView.z += ((1 - cosTheta) * y * z + x * sinTheta) * vView.y;
	vNewView.z += (cosTheta + (1 - cosTheta) * z * z)     * vView.z;

	m_vView = m_vPosition + vNewView;

	findCoord();
}

void Camera::findCoord()
{
	m_vZcoord = (m_vView - m_vPosition) / (m_vView - m_vPosition).Magnitude();
	m_vXcoord = (m_vUpVector * m_vZcoord) / (m_vUpVector * m_vZcoord).Magnitude();
	m_vYcoord = m_vZcoord * m_vXcoord;
}