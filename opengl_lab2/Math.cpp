#include "Math.h"

std::vector <Math::Vector3> Math::rotate(std::vector <Math::Vector3> vertices, 
	float angle, Math::Vector3 axis)
{

	if (axis.x == 1)
	{
		std::vector <Math::Vector3> rotateVertices;
		for (unsigned i = 0; i < vertices.size(); i++)
		{
			Math::Vector3 result;
			result.x = vertices[i].x;
			result.y = vertices[i].y *  std::cosf(angle) + vertices[i].z * std::sinf(angle);
			result.z = vertices[i].y * -std::sinf(angle) + vertices[i].z * std::cosf(angle);
			rotateVertices.push_back(result);
		}
		vertices = rotateVertices;
	}

	if (axis.y == 1)
	{
		std::vector <Math::Vector3> rotateVertices;
		for (unsigned i = 0; i < vertices.size(); i++)
		{
			Math::Vector3 result;
			result.x = vertices[i].x * std::cosf(angle) + vertices[i].z * -std::sinf(angle);
			result.y = vertices[i].y;
			result.z = vertices[i].x * std::sinf(angle) + vertices[i].z *  std::cosf(angle);
			rotateVertices.push_back(result);
		}
		vertices = rotateVertices;
	}

	if (axis.z == 1)
	{
		std::vector <Math::Vector3> rotateVertices;
		for (unsigned i = 0; i < vertices.size(); i++)
		{
			Math::Vector3 result;
			result.x = vertices[i].x *  std::cosf(angle) + vertices[i].y * std::sinf(angle);
			result.y = vertices[i].x * -std::sinf(angle) + vertices[i].y * std::cosf(angle);
			result.z = vertices[i].z;
			rotateVertices.push_back(result);
		}
		vertices = rotateVertices;
	}

	return vertices;
}

Math::Vector3 Math::rotateVertex(Math::Vector3 vertex,
	float angle, Math::Vector3 axis)
{

	if (axis.x == 1)
	{
		Math::Vector3 result;
		result.x = vertex.x;
		result.y = vertex.y *  std::cosf(angle) + vertex.z * std::sinf(angle);
		result.z = vertex.y * -std::sinf(angle) + vertex.z * std::cosf(angle);
		vertex = result;
	}

	if (axis.y == 1)
	{
		Math::Vector3 result;
		result.x = vertex.x * std::cosf(angle) + vertex.z * -std::sinf(angle);
		result.y = vertex.y;
		result.z = vertex.x * std::sinf(angle) + vertex.z *  std::cosf(angle);
		vertex = result;
	}

	if (axis.z == 1)
	{
		Math::Vector3 result;
		result.x = vertex.x *  std::cosf(angle) + vertex.y * std::sinf(angle);
		result.y = vertex.x * -std::sinf(angle) + vertex.y * std::cosf(angle);
		result.z = vertex.z;
		vertex = result;
	}

	return vertex;
}