#ifndef TEXTURE_H
#define TEXTURE_H

#include "common.h"

typedef unsigned int texID;

void LoadTextureFile(texID *texture, LPCTSTR lpFileName);

#endif /* TEXTURE_H */