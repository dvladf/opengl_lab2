#include "Object.h"

CObject::CObject()
{
}

void CObject::loadFromObjFile(std::string pathname)
{
	std::ifstream file(pathname);

	std::string header;
	
	while (!file.eof())
	{
		header = "";
		file >> header;

		if (header == "mtllib")
		{
			std::string path_mtl;
			file >> path_mtl;
			loadMtlFile(path_mtl);
		}

		else if (header == "o")
		{
			PartObject new_partObject;
			file >> new_partObject.name;
			partsObject.push_back(new_partObject);
		}

		else if (header == "usemtl")
		{
			std::string name_mtl;
			file >> name_mtl;
			for (unsigned i = 0; i < materials.size(); i++)
			{
				if (materials[i].name == name_mtl)
					partsObject[partsObject.size()-1].idMaterial = i;
			}
		}

		else if (header == "v")
		{
			Math::Vector3 vertex;
			file >> vertex.x >> vertex.y >> vertex.z;
			vertices.push_back(vertex);
			partsObject[partsObject.size() - 1].endNumVertex =
				vertices.size();
		}

		else if (header == "vt")
		{
			Math::Vector2 uv;
			file >> uv.x >> uv.y;
			uvs.push_back(uv);
		}

		else if (header == "vn")
		{
			Math::Vector3 normal;
			file >> normal.x >> normal.y >> normal.z;
			normals.push_back(normal);
		}

		else if (header == "f")
		{
			std::vector <unsigned> vertexIndex;
			std::vector <unsigned> uvIndex;
			std::vector <unsigned> normalIndex;

			std::string line;
			std::getline(file, line);

			std::istringstream line_stream(line);

			while (!line_stream.eof())
			{
				std::string substr;
				line_stream >> substr;
				std::istringstream substr_stream(substr);

				unsigned indices[3] = { 0, 0, 0 };

				std::string token;
				unsigned j = 0;
				while (std::getline(substr_stream, token, '/'))
				{
					std::istringstream token_stream(token);
					token_stream >> indices[j];
					j++;
				}

				vertexIndex.push_back(indices[0]);

				if (indices[1] != 0)
					uvIndex.push_back(indices[1]);

				if (indices[2] != 0)
					normalIndex.push_back(indices[2]);
			}


			for (unsigned i = 0; i < vertexIndex.size(); i++)
			{
				vertexIndices.push_back(vertexIndex[i] - 1);

				if (!uvIndex.empty())
					uvIndices.push_back(uvIndex[i] - 1);

				if (!normalIndex.empty())
					normalIndices.push_back(normalIndex[i] - 1);
			}

			partsObject[partsObject.size() - 1].endItemFace = vertexIndices.size();
		}
	}

	file.close();
}

void CObject::setTexture()
{
	//LoadTextureFile(&texture, MYFILE_TEXTURE_LANDSCAPE);
}

GLvoid CObject::render(std::vector <Math::Vector3> changesVertices)
{
	if (!vertices.empty())
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, 0, &changesVertices[0]);

		for (int i = 0; i < partsObject.size(); i++)
		{
			GLfloat *pColor = &materials[partsObject[i].idMaterial].mDiffuse_color.x;

			glColor3fv(pColor);

			glDrawElements(GL_TRIANGLES, partsObject[i].endItemFace, 
				GL_UNSIGNED_INT, &vertexIndices[0]);
		}

		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

void CObject::loadMtlFile(std::string filename)
{
	std::ifstream file(filename);
	std::string header;

	unsigned cur_it = 0;
	while (!file.eof())
	{

		header = "";
		file >> header;

		if (header == "newmtl")
		{
			cur_it = materials.size();
			Material new_material;
			file >> new_material.name;
			materials.push_back(new_material);
		}

		else if (header == "Ka")
		{
			file >> materials[cur_it].mLight_color.x >>
				materials[cur_it].mLight_color.y >>
				materials[cur_it].mLight_color.z;
		}

		else if (header == "Kd")
		{
			file >> materials[cur_it].mDiffuse_color.x >>
				materials[cur_it].mDiffuse_color.y >>
				materials[cur_it].mDiffuse_color.z;
		}

		else if (header == "Ks")
		{
			file >> materials[cur_it].mReflection_color.x >>
				materials[cur_it].mReflection_color.y >>
				materials[cur_it].mReflection_color.z;
		}

		else if (header == "Ns")
		{
			file >> materials[cur_it].mReflection_rate;
		}

		else if (header == "d")
		{
			file >> materials[cur_it].transparency;
		}
	}
}