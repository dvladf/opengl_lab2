#ifndef CAMERA_H
#define CAMERA_H

#include "common.h"

class Camera
{
public:
	Math::Vector3 m_vPosition;
	Math::Vector3 m_vView;
	Math::Vector3 m_vUpVector;

	Camera();
	~Camera();

	GLvoid PositionCamera(float positionX, float positionY, float positionZ,
		float viewX, float viewY, float viewZ,
		float upVectorX, float upVectorY, float upVectorZ);

	void  moveCamera(float height, float speed);
	void  rotateView(float angle, float x, float y, float z);

	Math::Vector3 m_vXcoord;
	Math::Vector3 m_vYcoord;
	Math::Vector3 m_vZcoord;

private:
	void findCoord();
};

#endif /* CAMERA_H */