#include "Skybox.h"

texID g_SkyboxBack;
texID g_SkyboxDown;
texID g_SkyboxFront;
texID g_SkyboxLeft;
texID g_SkyboxRight;
texID g_SkyboxUp;

void InitSkybox()
{
	LoadTextureFile(&g_SkyboxBack,  MYFILE_TEXTURE_SKYBOX_BACK);
	LoadTextureFile(&g_SkyboxDown,  MYFILE_TEXTURE_SKYBOX_DOWN);
	LoadTextureFile(&g_SkyboxFront, MYFILE_TEXTURE_SKYBOX_FRONT);
	LoadTextureFile(&g_SkyboxLeft,  MYFILE_TEXTURE_SKYBOX_LEFT);
	LoadTextureFile(&g_SkyboxRight, MYFILE_TEXTURE_SKYBOX_RIGHT);
	LoadTextureFile(&g_SkyboxUp,	MYFILE_TEXTURE_SKYBOX_UP);
}

GLvoid RenderSkybox(float x, float y, float z, float width, float height, float length)
{
	x -= width  / 2;
	y -= height / 2;
	z -= length / 2;

	glBindTexture(GL_TEXTURE_2D, g_SkyboxBack);
	glBegin(GL_QUADS);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x + width, y, z);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x + width, y + height, z);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x, y + height, z);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x, y, z);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, g_SkyboxFront);
	glBegin(GL_QUADS);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x, y, z + length);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x + width, y, z + length);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, g_SkyboxDown);
	glBegin(GL_QUADS);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x, y, z);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x, y, z + length);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x + width, y, z + length);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x + width, y, z);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, g_SkyboxUp);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x + width, y + height, z);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x, y + height, z);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, g_SkyboxLeft);
	glBegin(GL_QUADS);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x, y + height, z);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x, y, z + length);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x, y, z);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, g_SkyboxRight);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0f, 0.0f); glVertex3f(x + width, y, z);
	glTexCoord2d(1.0f, 0.0f); glVertex3f(x + width, y, z + length);
	glTexCoord2d(1.0f, 1.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2d(0.0f, 1.0f); glVertex3f(x + width, y + height, z);
	glEnd();
}