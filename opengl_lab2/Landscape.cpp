#include "Landscape.h"

static int  Height(BYTE *pHeightMap, int X, int Y);
static void SetTextureCoord(float x, float z);

texID g_TextureSurface;
texID g_TextureGrass;
Math::Point3i points[COUNT_GRASS];

// Global functions:

void InitLandscape(BYTE pHeightMap[])
{
	LoadTextureFile(&g_TextureSurface, MYFILE_TEXTURE_LANDSCAPE);
	LoadTextureFile(&g_TextureGrass,   MYFILE_TEXTURE_GRASS);

	for (int i = 0; i < COUNT_GRASS; i++)
	{
		points[i].x = rand() % MAP_SIZE;
		points[i].z = rand() % MAP_SIZE;
		points[i].y = int(FindHeight(pHeightMap, float(points[i].x), float(points[i].z)));
	}
}

GLvoid RenderSurface(BYTE pHeightMap[])
{
	int x, y, z;

	glBindTexture(GL_TEXTURE_2D, g_TextureSurface);

	for (int X_global = -MAP_SIZE; X_global <= MAP_SIZE; X_global += MAP_SIZE)
		for (int Y_global = -MAP_SIZE; Y_global <= MAP_SIZE; Y_global += MAP_SIZE)

			for (int X = 0; X <= MAP_SIZE; X += STEP_SIZE)
			{
				glBegin(GL_TRIANGLE_STRIP);
				for (int Y = 0; Y <= MAP_SIZE; Y += STEP_SIZE)
				{
					x = X + STEP_SIZE + X_global;
					y = -Height(pHeightMap, X + STEP_SIZE, Y);
					z = Y + Y_global;

					SetTextureCoord((float)x, (float)z);
					glVertex3i(x, y, z);

					x = X + X_global;
					y = -Height(pHeightMap, X, Y);
					z = Y + Y_global;

					SetTextureCoord((float)x, (float)z);
					glVertex3i(x, y, z);
				}
				glEnd();
			}
}

GLvoid RenderGrass(BYTE *pHeightMap, Math::Vector3 vRightVector, Math::Vector3 vUpVector)
{
	glBindTexture(GL_TEXTURE_2D, g_TextureGrass);

	for (int X_global = -MAP_SIZE; X_global <= MAP_SIZE; X_global += MAP_SIZE)
		for (int Y_global = -MAP_SIZE; Y_global <= MAP_SIZE; Y_global += MAP_SIZE)

			for (int i = 0; i < COUNT_GRASS; i++)
			{
				Math::Vector3 vPosition = { float(points[i].x) + X_global, 
					float(points[i].y), float(points[i].z) + Y_global};

				Math::Vector3 v0 = vPosition - vRightVector - vUpVector;
				Math::Vector3 v1 = vPosition - vRightVector + vUpVector;
				Math::Vector3 v2 = vPosition + vRightVector + vUpVector;
				Math::Vector3 v3 = vPosition + vRightVector - vUpVector;

				glBegin(GL_QUADS);
				glTexCoord2d(1.0f, 0.0f); glVertex3fv(&v0.x);
				glTexCoord2d(1.0f, 1.0f); glVertex3fv(&v1.x);
				glTexCoord2d(0.0f, 1.0f); glVertex3fv(&v2.x);
				glTexCoord2d(0.0f, 0.0f); glVertex3fv(&v3.x);
				glEnd();
			}
}

float FindHeight(BYTE *pHeightMap, float x, float z)
{
	float y;

	int X, Z;

	X = int(x / STEP_SIZE) * STEP_SIZE;
	Z = int(z / STEP_SIZE) * STEP_SIZE;

	Math::Triangle3i triangle;
	triangle.x1 = X;				 triangle.z1 = Z;
	triangle.x2 = X + STEP_SIZE;	 triangle.z2 = Z + STEP_SIZE;

	if ((z - Z) >= (x - X))
	{
		triangle.x3 = X;
		triangle.z3 = Z + STEP_SIZE;
	}

	else
	{
		triangle.x3 = X + STEP_SIZE;
		triangle.z3 = Z;
	}

	triangle.y1 = -Height(pHeightMap, triangle.x1, triangle.z1);
	triangle.y2 = -Height(pHeightMap, triangle.x2, triangle.z2);
	triangle.y3 = -Height(pHeightMap, triangle.x3, triangle.z3);

	y = -((x - triangle.x1) * (triangle.y2 - triangle.y1) * (triangle.z3 - triangle.z1) -
		(x - triangle.x1) * (triangle.z2 - triangle.z1) * (triangle.y3 - triangle.y1) -
		(z - triangle.z1) * (triangle.y2 - triangle.y1) * (triangle.x3 - triangle.x1) +
		(z - triangle.z1) * (triangle.x2 - triangle.x1) * (triangle.y3 - triangle.y1))
		/ ((triangle.z2 - triangle.z1) * (triangle.x3 - triangle.x1) -
		(triangle.x2 - triangle.x1) * (triangle.z3 - triangle.z1)) + triangle.y1;

	return y;
}

void LoadRawFile(LPCTSTR lpFileName, DWORD dwNumberOfBytesToRead, BYTE *pBuffer)
{
	HANDLE   hFile;
	BOOL	 bResult;
	DWORD    dwNumberOfBytesRead;

	hFile = CreateFile(lpFileName, GENERIC_READ, 0, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	bResult = ReadFile(hFile, pBuffer, dwNumberOfBytesToRead, &dwNumberOfBytesRead, NULL);
	CloseHandle(hFile);
}


// Local functions:

static int Height(BYTE *pHeightMap, int X, int Y)
{
	int x = X % MAP_SIZE;
	int y = Y % MAP_SIZE;

	if (!pHeightMap) exit(3);

	return pHeightMap[x + (y * MAP_SIZE)] / 5;
}

static void SetTextureCoord(float x, float z)
{
	glTexCoord2f((float)x / (float)MAP_SIZE * 20,
		-(float)z / (float)MAP_SIZE * 20);
}