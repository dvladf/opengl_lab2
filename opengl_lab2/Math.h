#ifndef MATH_H
#define MATH_H

#include <cmath>
#include <vector>

namespace Math
{
	const float deg2rad = 3.14159f / 180.0f;

	struct Triangle3i
	{
		int x1, y1, z1;
		int x2, y2, z2;
		int x3, y3, z3;
	};

	struct Point3i
	{
		int x, y, z;
	};

	class Vector2
	{
	public:
		Vector2() {}

		Vector2(float X, float Y)
		{
			x = X; y = Y;
		}

		float x, y;
	};

	class Vector3
	{
	public:
		Vector3() {}

		Vector3(float X, float Y, float Z)
		{
			x = X; y = Y; z = Z;
		}

		float Magnitude()
		{
			return (float)sqrt(x * x + y * y + z * z);
		}

		Vector3 operator+(Vector3 vVector)
		{
			return Vector3(vVector.x + x, vVector.y + y, vVector.z + z);
		}

		Vector3 operator-(Vector3 vVector)
		{
			return Vector3(x - vVector.x, y - vVector.y, z - vVector.z);
		}

		Vector3 operator*(Vector3 vVector)
		{
			return Vector3(y * vVector.z - z * vVector.y, z * vVector.x - x * vVector.z, x * vVector.y - y * vVector.x);
		}

		Vector3 operator*(float num)
		{
			return Vector3(x*num, y*num, z*num);
		}

		Vector3 operator/(float num)
		{
			return Vector3(x / num, y / num, z / num);
		}

		float x, y, z;
	};
	
	std::vector <Math::Vector3> rotate(std::vector<Math::Vector3> vertices, 
		float angle, Math::Vector3 axis);

	Math::Vector3 rotateVertex(Math::Vector3 vertex, float angle, 
		Math::Vector3 axis);
}


#endif /* MATH_H */